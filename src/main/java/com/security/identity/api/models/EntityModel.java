package com.security.identity.api.models;

/**
 * Interface used to map with json node.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
public interface EntityModel {
	
	public String getNode();

}
