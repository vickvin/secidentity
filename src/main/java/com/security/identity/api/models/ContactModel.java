package com.security.identity.api.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
@JsonInclude(Include.NON_EMPTY)
public class ContactModel implements EntityModel {
	
	@JsonProperty(access = Access.READ_ONLY)
	private Integer clientId;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String userName;
	
	@NotBlank
	@Size(max = 200)
	private String primaryAddress;
	
	@Size(max = 200)
	private String secondaryAddress;
	
	@NotBlank
	@Size(max = 15)
//	@JsonProperty(access = Access.WRITE_ONLY)
	private String phone;
	
	@NotBlank
	@Email
	//@JsonProperty(access = Access.WRITE_ONLY)
	private String primaryEmailId;
	
	@Email
	//@JsonProperty(access = Access.WRITE_ONLY)
	private String secondaryEmailId;
	
	@Override
	@JsonIgnore
	public String getNode() {
		return "contactInfo";
	}
	
}