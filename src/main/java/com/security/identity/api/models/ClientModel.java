package com.security.identity.api.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

/**
 * Model class used to map with json object.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
public class ClientModel implements EntityModel {

	@JsonProperty(access = Access.READ_ONLY)
	private Integer clientId;
	
	@NotBlank
	private String userName;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@NotBlank
	@Size(min = 6, max = 15)
	private String password;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@NotBlank
	private String confirmPassword;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String oldPassword;
	
	private boolean isPersonalDetails;
	
	private boolean isContactDetails;
	
	private boolean isPasswordSettings;
	
	@JsonProperty(access = Access.READ_ONLY)
	private PersonalModel personalInfo;
	
	@JsonProperty(access = Access.READ_ONLY)
	private ContactModel contactInfo;
	
	@Override
	@JsonIgnore
	public String getNode() {
		return "client";
	}
	
}