package com.security.identity.api.models;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

/**
 * Model class used for mapping with json object.
 * 
 * @author Shiva Jain
 * @since 2018-02-10
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
public class PersonSecInfoModel implements EntityModel {
	
	@JsonProperty(access = Access.READ_ONLY)
	private Integer clientId;
	
	@JsonProperty(access = Access.READ_ONLY)
	private Integer questionId;
	
	@NotBlank
	private String question;
	
	@NotBlank
	private String answer;
	
	@Override
//	@JsonIgnore
	public String getNode() {
		return "personSecurityInfo";
	}
}