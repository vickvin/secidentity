package com.security.identity.api.models;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

/**
 * Model class used for mapping with json object.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
public class PersonalModel implements EntityModel {
	
	@JsonProperty(access = Access.READ_ONLY)
	private Integer clientId;
	
	@JsonProperty(access = Access.READ_ONLY)
	private String userName;
	
	@NotBlank
	@Size(max = 50)
	private String firstName;
	
	@NotBlank
	@Size(max = 50)
	private String lastName;
	
	@Size(max = 50)
	private String middleName;

	@Size(max = 50)
	private String spouseFirstName;

	@Size(max = 50)
	private String spouseLastName;

	@Size(max = 50)
	private String spouseMiddleName;

	@Size(max = 3)
	private String age;

	@Override
	@JsonIgnore
	public String getNode() {
		return "personalInfo";
	}
}