package com.security.identity.api.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.ContactModel;
import com.security.identity.api.models.PersonSecInfoModel;
import com.security.identity.api.services.PersonSecurityInfoService;

/**
 * 
 * @author Shiva Jain
 * @since 2018-02-10
 * 
 * 
 */
@RestController
@RequestMapping(path = "/clients")
public class PersonSecurityInfoController {

	private static Logger logger = LoggerFactory.getLogger(PersonSecurityInfoController.class);

	@Autowired
	private PersonSecurityInfoService secInfoService;

	/**
	 * Add PersonSecurityInfo for a specific client
	 * 
	 * @param clientId
	 * @param secInfoModel
	 * @return
	 * @throws ApplicationException
	 */
	@PostMapping(value = "/{clientId}/securityInfo")
	public ResponseEntity<PersonSecInfoModel> createSecInfo(@PathVariable(value = "clientId") Integer clientId,
			@Valid @RequestBody PersonSecInfoModel secInfoModel) throws ApplicationException {

		logger.info("########## Create PersonSecurityInfo API begins ##########");
		PersonSecInfoModel model = secInfoService.createPersonSecurityInfo(secInfoModel, clientId);
		logger.info("########## Create PersonSecurityInfo API ends ##########");

		return new ResponseEntity<PersonSecInfoModel>(model, HttpStatus.CREATED);
	}

	/**
	 * @param clientId
	 * @param secInfoModel
	 * @return
	 * @throws ApplicationException
	 */
	@GetMapping(value = "/{clientId}/securityInfo/{questionId}")
	public ResponseEntity<PersonSecInfoModel> getSecInfoById(@PathVariable(value = "clientId") Integer clientId,
			@PathVariable(value = "questionId") Integer questionId) {

		logger.info("########## Get PersonSecurityInfo API begins ##########");

		PersonSecInfoModel model = secInfoService.getPersonSecurityInfoById(clientId, questionId);

		logger.info("########## Get PersonSecurityInfo API ends ##########");

		return new ResponseEntity<PersonSecInfoModel>(model, HttpStatus.OK);
	}

	/**
	 * @param clientId
	 * @param secInfoModel
	 * @return
	 * @throws ApplicationException
	 */
	@GetMapping(value = "/{clientId}/securityInfo")
	public ResponseEntity<ArrayList<PersonSecInfoModel>> getSecInfo(
			@PathVariable(value = "clientId") Integer clientId) {

		logger.info("########## Get PersonSecurityInfo API begins ##########");
		List<PersonSecInfoModel> modelList = secInfoService.getPersonSecurityInfo(clientId);
		ArrayList<PersonSecInfoModel> list = new ArrayList<>();
		for (PersonSecInfoModel personSecInfoModel : modelList) {
			list.add(personSecInfoModel);
		}
		logger.info("########## Get PersonSecurityInfo API ends ##########");

		return ResponseEntity.status(HttpStatus.OK).body(list);
	}

	/**
	 * @param clientId
	 * @param questionId
	 * @return
	 */
	@DeleteMapping(value = "/{clientId}/securityInfo/{questionId}")
	public ResponseEntity<Integer> deleteSecInfoById(@PathVariable(value = "clientId") Integer clientId,
			@PathVariable(value = "questionId") Integer questionId) {

		logger.info("########## Delete PersonSecurityInfo API begins ##########");

		secInfoService.deletePersonSecInfo(clientId, questionId);
		logger.info("########## Delete PersonSecurityInfo API ends ##########");

		return new ResponseEntity<Integer>(questionId, HttpStatus.ACCEPTED);
	}

	/**
	 * @param clientId
	 * @param questionId
	 * @param secInfoModel
	 * @return
	 * @throws ApplicationException
	 */
	@PutMapping(value = "/{clientId}/securityInfo/{questionId}")
	public ResponseEntity<PersonSecInfoModel> updateSecInfo(@PathVariable(value = "clientId") Integer clientId,
			@PathVariable(value = "questionId") Integer questionId, @Valid @RequestBody PersonSecInfoModel secInfoModel)
			throws ApplicationException {

		logger.info("########## Update PersonSecurityInfo API begins ##########");
		PersonSecInfoModel model = secInfoService.updatePersonSecurityInfo(secInfoModel, clientId, questionId);
		logger.info("########## Update PersonSecurityInfo API ends ##########");

		return new ResponseEntity<PersonSecInfoModel>(model, HttpStatus.OK);
	}

}