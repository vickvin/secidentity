package com.security.identity.api.controllers;

import java.io.IOException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.ContactModel;
import com.security.identity.api.services.ContactInfoService;

/**
 * Controller for performing operations
 * related with contactInfo.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 * 
 * 
 */
@RestController
@RequestMapping(path = "/clients")
public class ContactInfoController {
	
	private static Logger logger = LoggerFactory.getLogger(ContactInfoController.class);
	
	@Autowired
	private ContactInfoService contactService;
	
	/**
	 * Add contactInfo for a specific client
	 * 
	 *  
	 * @param clientId
	 * @param contactModel
	 * @return
	 * @throws ApplicationException
	 */
	@PostMapping(value = "/{clientId}/contactInfo")
	public ResponseEntity<ContactModel> createContactInfo(@PathVariable(value = "clientId") Integer clientId,
			@Valid @RequestBody ContactModel contactModel) throws ApplicationException {
		
		logger.info("########## Create ContactInfo API begins ##########");
		ContactModel model = contactService.createContactInfo(contactModel, clientId);
		logger.info("########## Create ContactInfo API ends ##########");

		return new ResponseEntity<ContactModel>(model, HttpStatus.CREATED);
	}
	
	/**
	 * Update contactInfo for a specific client
	 * 
	 * 
	 * @param clientId
	 * @param contactJson
	 * @return
	 * @throws ApplicationException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PutMapping(value = "/{clientId}/contactInfo")
	public ResponseEntity<ContactModel> updateContactInfo(@PathVariable(value = "clientId") Integer clientId, 
			@RequestBody String contactJson) throws ApplicationException, JsonProcessingException, IOException {
		
		logger.info("########## Update ContactInfo API begins ##########");
		ContactModel model = contactService.updateContactInfo(contactJson, clientId);
		logger.info("########## Update ContactInfo API ends ##########");

		return new ResponseEntity<ContactModel>(model, HttpStatus.OK);
		
	}

}