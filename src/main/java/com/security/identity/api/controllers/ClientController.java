package com.security.identity.api.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.security.identity.api.models.ClientModel;
import com.security.identity.api.response.ApiResponse;
import com.security.identity.api.services.ClientService;

/**
 * Controller for performing operations 
 * related with client 
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 * 
 * 
 */
@RestController
@RequestMapping(path = "/clients")
public class ClientController {

	private static Logger logger = LoggerFactory.getLogger(ClientController.class);

	@Autowired
	private ClientService clientService;
	
	/**
	 * Create/register/signup a client into the application
	 * 
	 * @param clientModel
	 * @return
	 */
	@PostMapping
	public ResponseEntity<ClientModel> createClient(@Valid @RequestBody ClientModel clientModel) {

		logger.info("########## Create Client API begins ##########");
		ClientModel client = clientService.createClient(clientModel);
		logger.info("########## Create Client API ends ##########");

		return new ResponseEntity<ClientModel>(client, HttpStatus.CREATED);
	}

	/**
	 * Get client for a given clientId from DB
	 * 
	 * @param clientId
	 * @return
	 */
	@GetMapping(value = "/{clientId}")
	public ResponseEntity<ClientModel> getClient(@PathVariable(value = "clientId") Integer clientId) {
		
		logger.info("########## Get Client API begins ##########");
		ClientModel client = clientService.getClient(clientId);
		logger.info("########## Get Client API ends ##########");

		return new ResponseEntity<ClientModel>(client, HttpStatus.OK);
	}
	
	/**
	 * Delete specific client from DB
	 * 
	 * @param clientId
	 * @return
	 */
	@DeleteMapping(value = "/{clientId}")
	public ResponseEntity<Integer> deleteClient(@PathVariable(value = "clientId") Integer clientId) {
		
		logger.info("########## Delete Client API begins ##########");
		clientService.deleteClient(clientId);
		logger.info("########## Delete Client API ends ##########");

		return new ResponseEntity<Integer>(clientId, HttpStatus.ACCEPTED);
	}
	
	/**
	 * Get authenticated client
	 * 
	 * @param client
	 * @return
	 */
	@GetMapping
	public ResponseEntity<ApiResponse> client(Principal client) {
		
		ApiResponse response = clientService.getClient(client);
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}
	
}