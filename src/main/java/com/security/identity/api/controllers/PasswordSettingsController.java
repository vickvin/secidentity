package com.security.identity.api.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.security.identity.api.models.ClientModel;
import com.security.identity.api.response.ApiResponse;
import com.security.identity.api.services.PasswordSettingsService;

/**
 * Controller for performing operations
 * related with password settings.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 * 
 * 
 */
@RestController
@RequestMapping(path = "/clients")
public class PasswordSettingsController {

	private static Logger logger = LoggerFactory.getLogger(PasswordSettingsController.class);

	@Autowired
	private PasswordSettingsService settingsService;

	/**
	 * API to reset/change password for a specific client.
	 * 
	 * @param clientId
	 * @param clientModel
	 * @return
	 */
	@PutMapping(value = "/passwordSettings")
	public ResponseEntity<ApiResponse> modifyPassword(@Valid @RequestBody ClientModel clientModel) {

		logger.info("########## Modify Client Password API begins ##########");
		ApiResponse response = settingsService.updatePassword(clientModel);
		logger.info("########## Modify Client Password API ends ##########");

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

}