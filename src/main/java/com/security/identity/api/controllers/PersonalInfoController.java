package com.security.identity.api.controllers;

import java.io.IOException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.PersonalModel;
import com.security.identity.api.services.PersonalInfoService;

/**
 * Controller for performing operations
 * related with personalInfo.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 * 
 * 
 */
@RestController
@RequestMapping(path = "/clients")
public class PersonalInfoController {
	
	private static Logger logger = LoggerFactory.getLogger(PersonalInfoController.class);
	
	@Autowired
	private PersonalInfoService personalService;
	
	/**
	 * Add personalInfo for a specific client
	 * 
	 * @param clientId
	 * @param personalInfo
	 * @return
	 * @throws ApplicationException
	 */
	@PostMapping(value = "/{clientId}/personalInfo")
	public ResponseEntity<PersonalModel> createPersonalInfo(@PathVariable(value = "clientId") Integer clientId,
			@Valid @RequestBody PersonalModel personalModel) throws ApplicationException {
		
		logger.info("########## Create PersonalInfo API begins ##########");
		PersonalModel model = personalService.createPersonalInfo(personalModel, clientId);
		logger.info("########## Create PersonalInfo API ends ##########");

		return new ResponseEntity<PersonalModel>(model, HttpStatus.CREATED);
	}
	
	/**
	 * Update personalInfo for a specific client
	 * 
	 * @param clientId
	 * @param personalInfo
	 * @return
	 * @throws ApplicationException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PutMapping(value = "/{clientId}/personalInfo")
	public ResponseEntity<PersonalModel> updatePersonalInfo(@PathVariable(value = "clientId") Integer clientId, 
			@RequestBody String personalJson) throws ApplicationException, JsonProcessingException, IOException {
		
		logger.info("########## Update PersonalInfo API begins ##########");
		PersonalModel model = personalService.updatePersonalInfo(personalJson, clientId);
		logger.info("########## Update PersonalInfo API ends ##########");

		return new ResponseEntity<PersonalModel>(model, HttpStatus.OK);
		
	}

}