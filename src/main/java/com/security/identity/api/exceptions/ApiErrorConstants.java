package com.security.identity.api.exceptions;

/**
 * To make an API's error uniquely identifiable by its error constants.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
public enum ApiErrorConstants {
	
	VALIDATION_FAILED,
    REQUIRED_PARAMETER,
    PASSWORDS_NOT_SAME,
    SUCCESSFULLY_UPDATED,
    SUCCESSFULLY_DELETED,
    ERROR_WRITING_JSON_OUTPUT,
    USER_ALREADY_EXISTS,
    PASSWORD_NOT_EMPTY,
    ENTITY_NOT_FOUND,
    INVALID_PASSWORD,
    UNAUTHORIZED_SERVICE,
    ENCRYPTION_ERROR,
    DECRYPTION_ERROR,
    RESOURCE_NOT_ACCESSIBLE,
    INVALID_CREDENTIALS,
    CREDENTIALS_MANDATORY,
    NOT_ALLOWED;

}