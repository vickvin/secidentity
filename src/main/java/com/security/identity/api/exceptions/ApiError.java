package com.security.identity.api.exceptions;

import static com.security.identity.api.ApplicationConstants.VALID_FORMAT_YYYY_MM_DD;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Wrapper class to wrap all the exceptions.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
public class ApiError {
	
	private String apiVersion = "v1";
	private String status;
	private int code;
	private String timestamp;
	private String message;
	private String exceptionDetail;
	private List<ErrorDetail> errorDetails;
	
	/**
	 * 
	 */
    private ApiError() {
        timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(VALID_FORMAT_YYYY_MM_DD));
    }

    /**
     * @param status
     */
    public ApiError(HttpStatus status) {
        this();
        this.status = status.name();
        this.code = status.value();
    }
	
    /**
     * 
     * @param status
     * @param ex
     */
	public ApiError(HttpStatus status, Throwable ex) {
		this(status);
		this.exceptionDetail = ex.getLocalizedMessage();
	}
	
	/**
	 * 
	 * @param errors
	 */
	public void addErrorDetails(List<FieldError> errors) {
		errors.forEach(this::addErrorDetail);
	}
	
	/**
	 * 
	 * @param error
	 */
	public void addErrorDetail(FieldError error) {
		this.addErrorDetail(error.getObjectName(), error.getField(), error.getRejectedValue(), error.getDefaultMessage());
	}
	
	/**
	 * 
	 * @param object
	 * @param field
	 * @param rejectedValue
	 * @param meesage
	 */
	public void addErrorDetail(String object, String field, Object rejectedValue, String meesage) {
		this.addErrorDetail(new ErrorDetail(object, field, rejectedValue, meesage));
	}
	
	/**
	 * 
	 * @param errorDetail
	 */
	public void addErrorDetail(ErrorDetail errorDetail) {
		if (this.errorDetails == null) {
			this.errorDetails = new ArrayList<ErrorDetail>();
		}
		this.errorDetails.add(errorDetail);
	}
	
	/**
     * Utility method for adding error of ConstraintViolation.
     * 
     * @param cv the ConstraintViolation
     */
    private void addErrorDetail(ConstraintViolation<?> cv) {
        this.addErrorDetail(
                cv.getRootBeanClass().getSimpleName(),
                ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
                cv.getInvalidValue(),
                cv.getMessage());
    }

    /**
     * @param constraintViolations
     */
    void addErrorDetail(Set<ConstraintViolation<?>> constraintViolations) {
        constraintViolations.forEach(this::addErrorDetail);
    }
	
	@Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class ErrorDetail {
        private String object;
        private String field;
        private Object rejectedValue;
        private String message;

        ErrorDetail(String object, String message) {
            this.object = object;
            this.message = message;
        }

        ErrorDetail(String message) {
            this.message = message;
        }
    }

}