package com.security.identity.api.exceptions;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

/**
 * Helper to simplify accessing i18n messages in code.
 * This finds messages automatically found from 
 * src/main/resources (files named messages_*.properties)
 * 
 *
 * @author Shubham Solanki
 * @since 2018-02-10
 */
@Component
public class MessageSourceHandler {

    @Autowired
    private MessageSource messageSource;

    private MessageSourceAccessor accessor;

    /**
     * 
     */
    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource, Locale.ENGLISH);
    }

    /**
     * 
     * @param code
     * @return
     */
    public String get(String code) {
        try {
            return accessor.getMessage(code);
        } catch (NoSuchMessageException ex ) {
            return code; 
        }
    }

}