package com.security.identity.api.exceptions;

import static com.security.identity.api.response.MessageUtil.getMessageFromBundle;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This class is a single global exception handler component wrapping for all 
 * the exceptions from APIs, prepare a Response object with required exception 
 * details sending back to users.
 * 
 * @ControllerAdvice  
 * @RestController
 * @author Shubham Solanki
 * @since   2018-02-10 
 * 
 *
 */
@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSourceHandler messageHandler;

	/**
     * Handle MissingServletRequestParameterException when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ApiError apiError = new ApiError(BAD_REQUEST, ex);
		apiError.setMessage(
				getMessageFromBundle(messageHandler, ApiErrorConstants.REQUIRED_PARAMETER, ex.getParameterName()));
		return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
	}

	/**
     * Handle MethodArgumentNotValidException an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
		apiError.setMessage(messageHandler.get(ApiErrorConstants.VALIDATION_FAILED.name()));
		apiError.addErrorDetails(ex.getBindingResult().getFieldErrors());
		return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
	}
	
	/**
     * Handle javax.validation.ConstraintViolationException.
     *
     * @param ex the ConstraintViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(javax.validation.ConstraintViolationException ex) {

        ApiError apiError = new ApiError(BAD_REQUEST, ex);

        apiError.addErrorDetail(ex.getConstraintViolations());
        apiError.setMessage(messageHandler.get(ApiErrorConstants.VALIDATION_FAILED.name()));
        return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
    }

	/**
     * Handle generic ApplicationException.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
	@ExceptionHandler(ApplicationException.class)
	public ResponseEntity<Object> handleApiBadRequest(ApplicationException ex) {
		ApiError apiError = new ApiError(ex.getStatus(), ex);
		apiError.setMessage(getMessageFromBundle(messageHandler, ex.getErrorConstants(), ex.getErrorMessageParams()));
		return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
	}
	
	/**
     * Handle generic {@link BadCredentialsException}
     *
     * @param ex the Exception
     * @return the ApiError object
     */
	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException ex) {
		ApiError apiError = new ApiError(UNAUTHORIZED);
		apiError.setMessage(getMessageFromBundle(messageHandler, ApiErrorConstants.INVALID_CREDENTIALS));
		return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
	}
	
	/**
     * Handle generic {@link InsufficientAuthenticationException}
     *
     * @param ex the Exception
     * @return the ApiError object
     */
	@ExceptionHandler(InsufficientAuthenticationException.class)
	public ResponseEntity<Object> handleInsufficientAuthenticationException(InsufficientAuthenticationException ex) {
		ApiError apiError = new ApiError(UNAUTHORIZED);
		apiError.setMessage(getMessageFromBundle(messageHandler, ApiErrorConstants.CREDENTIALS_MANDATORY));
		return new ResponseEntity<Object>(apiError, HttpStatus.valueOf(apiError.getCode()));
	}

}