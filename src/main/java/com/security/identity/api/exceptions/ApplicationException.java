package com.security.identity.api.exceptions;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Custom exception class to handle exceptions related with Application
 * 
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApplicationException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private ApiErrorConstants errorConstants;
	
	private String[] errorMessageParams;
	
	private HttpStatus status;
	
	/**
	 * 
	 * @param message
	 */
	public ApplicationException(String message) {
		super(message);
	}
	
	/**
	 * 
	 * @param errorConstants
	 * @param params
	 */
	public ApplicationException(ApiErrorConstants errorConstants, String...params) {
		this.errorConstants = errorConstants;
		this.errorMessageParams = params;
	}
	
	/**
	 * 
	 * @param errorConstants
	 * @param status
	 * @param params
	 */
	public ApplicationException(ApiErrorConstants errorConstants, HttpStatus status, String...params) {
		this(errorConstants, params);
		this.setStatus(status); 
	}
	
	/**
	 * 
	 * @param errorConstants
	 * @param params
	 * @return
	 */
	public static ApplicationException createBadRequest(ApiErrorConstants errorConstants, String...params) {
		ApplicationException applicationException = new ApplicationException(errorConstants, params);
		applicationException.setStatus(HttpStatus.BAD_REQUEST);
		return applicationException;
	}
	
	/**
	 * 
	 * @param errorConstant
	 * @param params
	 * @return
	 */
	public static ApplicationException createEntityNotFoundError(ApiErrorConstants errorConstant, String...params) {
        ApplicationException appException = new ApplicationException(errorConstant, params);
        appException.setStatus(HttpStatus.NOT_FOUND);
        return appException;
    }
	
	/**
	 * 
	 * @param errorConstant
	 * @param params
	 * @return
	 */
	public static ApplicationException createInternalError(ApiErrorConstants errorConstant, String...params) {
        ApplicationException appException = new ApplicationException(errorConstant, params);
        appException.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return appException;
    }
	
	/**
	 * 
	 * @param errorConstant
	 * @param params
	 * @return
	 */
	public static ApplicationException createUnauthorizedError(ApiErrorConstants errorConstant, String...params) {
        ApplicationException appException = new ApplicationException(errorConstant, params);
        appException.setStatus(HttpStatus.UNAUTHORIZED);
        return appException;
    }

	/**
	 * 
	 * @param errorConstant
	 * @param params
	 * @return
	 */
	public static ApplicationException createEncryptionError(ApiErrorConstants errorConstant, String...params) {
		ApplicationException appException = new ApplicationException(errorConstant, params);
        appException.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return appException;
	}
	
}