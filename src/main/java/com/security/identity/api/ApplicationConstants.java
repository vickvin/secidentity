package com.security.identity.api;

/**
 * Class to keep all the constants used by application
 * 
 * @author Shubham Solanki
 * @since 2018-23-02
 *
 */
public class ApplicationConstants {

	public static final String VALID_FORMAT_YYYY_MM_DD = "yyyy-MM-dd HH:mm:ss";
	
	public static final String AES_PKC_PADDING = "AES/CBC/PKCS5PADDING";
	public static final String AES_ALGO = "AES";
	public static final String UTF8 = "UTF-8";

}
