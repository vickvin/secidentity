package com.security.identity.api.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.security.identity.api.util.CommonUtil;

/**
 * Interceptor to log each API request's start time, end time and total time
 * taken to process request
 * 
 * @author Shubham Solanki
 * @since 2018-23-02
 *
 */
public class ApiTimeEvaluatingInterceptor extends HandlerInterceptorAdapter {

	private static Logger logger = LoggerFactory.getLogger(ApiTimeEvaluatingInterceptor.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
	 * preHandle(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		long startTime = System.currentTimeMillis();

		logger.info(" Request URL::" + request.getRequestURL().toString());
		logger.info("API execution Start Time " + CommonUtil.getTodayInUTC());

		request.setAttribute("startTime", startTime);

		// if returned false, we need to make sure 'response' is sent
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
	 * afterCompletion(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object,
	 * java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		logger.info("API execution End Time " + CommonUtil.getTodayInUTC());
	}

}