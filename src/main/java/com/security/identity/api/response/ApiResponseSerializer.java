package com.security.identity.api.response;

import java.io.IOException;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.EntityModel;

/**
 * Custom serializer to handle json attribute name for different data
 * 
 * @author Shubham Solanki
 * @Since 2018-02-10
 *
 */
public class ApiResponseSerializer extends JsonSerializer<ApiResponse> {

    /* (non-Javadoc)
     * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object, com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.SerializerProvider)
     */
    @Override
    public void serialize(ApiResponse apiResponse,
            JsonGenerator jGen,
            SerializerProvider serializerProvider) {
        try {

            jGen.writeStartObject();

            serializeResponseStatus(apiResponse, jGen);
            
            serializeMessage(apiResponse, jGen);
            
            serializeAuthenticatedUser(apiResponse, jGen);

            serializeSearchEntity(apiResponse, jGen);
            
            serializeEntityList(apiResponse, jGen);

            jGen.writeEndObject();
        } catch (Exception ex) {
            throw ApplicationException.createInternalError(ApiErrorConstants.ERROR_WRITING_JSON_OUTPUT, apiResponse.toString());
        }
    }

    /**
     * To serialize authenticated user
     * 
     * @param apiResponse
     * @param jGen
     * @throws IOException
     */
    private void serializeAuthenticatedUser(ApiResponse apiResponse, JsonGenerator jGen) throws IOException {
    	if (apiResponse.getClientId() != null) {
    		jGen.writeNumberField("clientId", apiResponse.getClientId()); 
    	}
    	if (!StringUtils.isEmpty(apiResponse.getUserName())) {
            jGen.writeStringField("userName", apiResponse.getUserName());
        }
	}

    /**
     * To serialize message
     * 
     * @param apiResponse
     * @param jGen
     * @throws IOException
     */
    private void serializeMessage(ApiResponse apiResponse, JsonGenerator jGen) throws IOException {
        if (!StringUtils.isEmpty(apiResponse.getMessage())) {
            jGen.writeStringField("message", apiResponse.getMessage());
        }
    }

    /**
     * To serialize response status
     * 
     * @param apiResponse
     * @param jGen
     * @throws IOException
     */
    private void serializeResponseStatus(ApiResponse apiResponse, JsonGenerator jGen) throws IOException {
        jGen.writeStringField("status", apiResponse.getStatus());
        jGen.writeStringField("code", apiResponse.getCode());
    }

    /**
     * To serialize entity
     * 
     * @param apiResponse
     * @param jGen
     * @throws IOException
     */
    private void serializeSearchEntity(ApiResponse apiResponse, JsonGenerator jGen) throws IOException {
        if (apiResponse.getEntity() != null) {
            jGen.writeFieldName(apiResponse.getEntity().getNode());
            jGen.writeObject(apiResponse.getEntity());
        }
    }
    
    /**
     * To serialize list
     * 
     * @param apiResponse
     * @param jGen
     * @throws IOException
     */
    private void serializeEntityList(ApiResponse apiResponse, JsonGenerator jGen) throws IOException {
        if (apiResponse.getList() != null) {
            jGen.writeFieldName(((EntityModel)apiResponse.getList().get(0)).getNode());
            jGen.writeObject(apiResponse.getList());
        }
    }

}