package com.security.identity.api.response;

import static com.security.identity.api.response.MessageUtil.getMessageFromBundle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.security.identity.api.exceptions.ApiError;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.MessageSourceHandler;
import com.security.identity.api.models.EntityModel;

/**
 * Global response handler component wrapping all responses from APIs, 
 * prepare a wrapper over response object and put additional informations e.g., status and code 
 * which are useful for API consumer.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@ControllerAdvice ("com.security.identity.api.controllers")
public class AppResponseHandler implements ResponseBodyAdvice<Object> {
	
	@Autowired
	private MessageSourceHandler messageHandler;
	
	/**
	 * 
	 * @param returnType
	 * @param converterType
	 * @return  
	 */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}
	
	/**
     * This method is to identify response body and inject additional information to response. 
     * 
     *  @param body
     *  @param returnType
     *  @param selectedContenctType
     *  @param selectedConverterType
     *  @param request
     *  @param response
     */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		if (body instanceof ApiError || body instanceof Exception) {
			return body;
		}
		
        ServletServerHttpResponse httpResponse = (ServletServerHttpResponse) response;
        
        ApiResponse apiResponse = new ApiResponse();
        
        if (body instanceof ApiResponse) {
        	apiResponse = (ApiResponse) body;
        }
        
        int statusCode = httpResponse.getServletResponse().getStatus();
        apiResponse.setCode(String.valueOf(statusCode));
        apiResponse.setStatus(HttpStatus.valueOf(statusCode).name());
        
        if (body instanceof EntityModel) {
        	apiResponse.setEntity((EntityModel)body);
        }
        if (body instanceof Integer && statusCode == HttpStatus.ACCEPTED.value()) {
        	apiResponse.setMessage(getMessageFromBundle(messageHandler, ApiErrorConstants.SUCCESSFULLY_DELETED, "id="+body.toString()));
        }
        
        if (body instanceof List) {
        	apiResponse.setList((List)body);
        }
       
		return apiResponse;
	}

}