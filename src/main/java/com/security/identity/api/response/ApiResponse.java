package com.security.identity.api.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.security.identity.api.models.EntityModel;

import lombok.Data;

/**
 * Global response object to wrap response from all the APIs and return additional attributes.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
@JsonInclude(Include.NON_EMPTY)
@JsonSerialize(using = ApiResponseSerializer.class)
public class ApiResponse {
	
	private String status;
	private String code;
	private EntityModel entity;
	private String message;
	private Integer clientId;
	private String userName;
	@SuppressWarnings("rawtypes")
	private List list;

}
