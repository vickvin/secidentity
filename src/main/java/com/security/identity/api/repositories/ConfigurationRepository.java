package com.security.identity.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.identity.api.entities.Configuration;

/**
 * Repository for user_configuration entity
 * 
 * @author Shubham Solanki		
 * @since 2018-02-10
 *
 */
public interface ConfigurationRepository extends JpaRepository<Configuration, Integer> {

}
