package com.security.identity.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.entities.PersonSecurityInfo;

/**
 * Repository for personalInfo entity
 * 
 * @author Shiva Jain
 * @since 2018-02-10
 *
 */
public interface PersonSecurityInfoRepository extends JpaRepository<PersonSecurityInfo, Integer> {

	List<PersonSecurityInfo> findByClient(ClientAuthentication client);
	
	

}