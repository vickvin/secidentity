package com.security.identity.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.identity.api.entities.ContactInfo;

/**
 * Repository for contactInfo entity
 * 
 * @author Shubham Solanki		
 * @since 2018-02-10
 *
 */
public interface ContactInfoRepository extends JpaRepository<ContactInfo, Integer> {

}
