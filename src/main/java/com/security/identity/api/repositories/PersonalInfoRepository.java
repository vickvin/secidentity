package com.security.identity.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.identity.api.entities.PersonalInfo;

/**
 * Repository for personalInfo entity
 * 
 * @author Shubham Solanki		
 * @since 2018-02-10
 *
 */
public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, Integer> {

}
