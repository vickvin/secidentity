package com.security.identity.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.identity.api.entities.ClientAuthentication;

/**
 * Repository for authentication entity
 * 
 * @author Shubham Solanki		
 * @since 2018-02-10
 *
 */
public interface ClientRepository extends JpaRepository<ClientAuthentication, Integer> {
	
	public ClientAuthentication findByUserName(String userName);

}
