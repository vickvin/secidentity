package com.security.identity.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * Database entity object for contactInfo
 * 
 * Name of database table is contact_info
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Entity
@Table(name = "contact_info")
@Data
public class ContactInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "contact_id")
	private Integer contactId;

	@Column(name = "address1", nullable = false)
	private String address1;
	
	@Column(name = "address2")
	private String address2;
	
	@Column(name = "phone", nullable = false)
	private String phone;
	
	@Column(name = "email_id1", nullable = false)
	private String emailId1;
	
	@Column(name = "email_id2")
	private String emailId2;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private ClientAuthentication client;
	
}