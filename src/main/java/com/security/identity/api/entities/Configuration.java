package com.security.identity.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * Database entity object for configuration
 * 
 * Name of database table is user_configuration
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Entity
@Table(name = "user_configuration")
@Data
public class Configuration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "config_id")
	private Integer configurationId;
	
	@Column(name = "user_personal_info")
	private Integer personalInfo;
	
	@Column(name = "user_contact_info")
	private Integer contactInfo;
	
	@Column(name = "password_settings")
	private Integer passwordSettings;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private ClientAuthentication client;

}