package com.security.identity.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * Database entity object for personalInfo
 * 
 * Name of database table is personal_info
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Entity
@Table(name = "personal_info")
@Data
public class PersonalInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Column(name = "last_name", nullable = false)
	private String lastName;
	
	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "spouse_first_name")
	private String spouseFirstName;

	@Column(name = "spouse_last_name")
	private String spouseLastName;

	@Column(name = "spouse_middle_name")
	private String spouseMiddleName;

	@Column(name = "age")
	private String age;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private ClientAuthentication client;
	
}