package com.security.identity.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * Database entity object for personalInfo
 * 
 * Name of database table is personal_info
 * 
 * @author Shiva Jain
 * @since 2018-02-10
 *
 */
@Entity
@Table(name = "person_security_info")
@Data
public class PersonSecurityInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "question", nullable = false)
	private String question;
	
	@Column(name = "answer", nullable = false)
	private String answer;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private ClientAuthentication client;
	
}