package com.security.identity.api.services;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.entities.Configuration;
import com.security.identity.api.models.ClientModel;
import com.security.identity.api.response.ApiResponse;

/**
 * Provides a collection of all services related with authentication
 * database object
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Service
public class ClientService extends CommonService {
	
	@Autowired
	private ContactInfoService contactService;
	
	@Autowired
	private PersonalInfoService personalService;

	/**
	 * Method to delete specific client
	 * 
	 * @param clientId
	 */
	@Transactional
	public void deleteClient(Integer clientId) {
		ClientAuthentication client = validateClient(clientId);
		clientRepository.delete(client);
	}

	/**
	 * Method to get specific client from DB
	 * 
	 * @param clientId
	 * @return
	 */
	public ClientModel getClient(Integer clientId) {
		ClientAuthentication client = validateClient(clientId);
		return convertClientEntityToModel(client);
	}

	/**
	 * Method to convert client entity to client model.
	 * 
	 * @param client
	 * @return
	 */
	private ClientModel convertClientEntityToModel(ClientAuthentication client) {
		ClientModel model = new ClientModel();
		model.setClientId(client.getClientId());
		if (client.getConfiguration().getPersonalInfo() == 1) {
			model.setPersonalDetails(true);
		} else {
			model.setPersonalDetails(false); 
		}
		if (client.getConfiguration().getContactInfo() == 1) {
			model.setContactDetails(true);
		} else {
			model.setContactDetails(false);
		}
		if (client.getConfiguration().getPasswordSettings() == 1) {
			model.setPasswordSettings(true);
		} else {
			model.setPasswordSettings(false);
		}
		model.setUserName(client.getUserName());
		if (client.getPersonalInfo() != null) {
			model.setPersonalInfo(personalService.convertPersonalEntityToModel(client.getPersonalInfo(), null));
		}
		if (client.getContactInfo() != null) {
			model.setContactInfo(contactService.convertContactEntityToModel(client.getContactInfo(), null));
		}
		return model;
	}

	/**
	 * Method to create client into the system
	 * 
	 * @param clientModel
	 * @return
	 */
	@Transactional
	public ClientModel createClient(ClientModel clientModel) {
		
		validateUserName(clientModel.getUserName());
		
		validatePassword(clientModel, null, false);
		
		ClientAuthentication client = convertClientModelToEntity(clientModel);
		
		ClientAuthentication savedClient = clientRepository.save(client);
		clientModel.setClientId(savedClient.getClientId());
		
		// Saving configuration for this client
		createConfigurationEntity(clientModel, savedClient);
		return clientModel;
	}
	
	/**
	 * Method to create user_configuration entity
	 * 
	 * @param clientModel
	 * @param client
	 * @return
	 */
	public Configuration createConfigurationEntity(ClientModel clientModel, ClientAuthentication client) {
		Configuration configuration = new Configuration();
		if (clientModel.isPersonalDetails()) {
			configuration.setPersonalInfo(1);
		} else {
			configuration.setPersonalInfo(0);
		}
		if (clientModel.isContactDetails()) {
			configuration.setContactInfo(1);
		} else {
			configuration.setContactInfo(0);
		}
		if (clientModel.isPasswordSettings()) {
			configuration.setPasswordSettings(1);
		} else {
			configuration.setPasswordSettings(0);
		}
		configuration.setClient(client);  
		return configRepository.save(configuration);
	}
	
	/**
	 * Method to convert client model to client entity 
	 *  
	 * @param clientModel
	 * @return
	 */
	public ClientAuthentication convertClientModelToEntity(ClientModel clientModel) {
		ClientAuthentication client = new ClientAuthentication();
		client.setUserName(clientModel.getUserName()); 
		client.setPassword(encoder.encode(clientModel.getPassword()));  
		return client;
	}

	/**
	 * Method to get authenticated client details
	 * 
	 * @param client
	 * @return
	 */
	public ApiResponse getClient(Principal client) {
		UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) client;
		User user = (User)authToken.getPrincipal();
		ClientAuthentication clientEntity = clientRepository.findByUserName(user.getUsername());
		
		ApiResponse response = new ApiResponse();
		response.setClientId(clientEntity.getClientId());
		response.setUserName(clientEntity.getUserName()); 
		return response; 
	}

}