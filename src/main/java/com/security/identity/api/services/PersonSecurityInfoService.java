package com.security.identity.api.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.entities.PersonSecurityInfo;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.PersonSecInfoModel;

/**
 *
 * @author Shiva Jain
 * @Since 2018-02-10
 * 
 */
@Service
public class PersonSecurityInfoService extends CommonService {

	/**
	 * Method to create SecurityInfo for a specific client
	 * 
	 * @param secInfoModel
	 * @param clientId
	 * @return
	 */

	public PersonSecInfoModel createPersonSecurityInfo(PersonSecInfoModel secInfoModel, Integer clientId) {
		ClientAuthentication client = validateClient(clientId);

		if (client == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal",
					String.valueOf(clientId));
		}
		PersonSecurityInfo securityInfo = new PersonSecurityInfo();
		securityInfo.setClient(client);
		securityInfo.setQuestion(encryptor.encrypt(secInfoModel.getQuestion()));
		securityInfo.setAnswer(encryptor.encrypt(secInfoModel.getAnswer()));
		PersonSecurityInfo personSecurityInfo = secInfoRepository.save(securityInfo);
		secInfoModel.setClientId(personSecurityInfo.getId());
		secInfoModel.setQuestionId(personSecurityInfo.getId());
		return secInfoModel;
	}

	/**
	 * @param clientId
	 * @param questionId
	 * @return
	 */
	public PersonSecInfoModel getPersonSecurityInfoById(Integer clientId, Integer questionId) {
		ClientAuthentication client = validateClient(clientId);

		if (client == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal",
					String.valueOf(clientId));
		}

		PersonSecurityInfo entity = secInfoRepository.findOne(questionId);

		if (entity == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.ENTITY_NOT_FOUND, "security info",
					String.valueOf(questionId));
		}

		PersonSecInfoModel model = new PersonSecInfoModel();
		model.setQuestion(encryptor.decrypt(entity.getQuestion()));
		model.setAnswer(encryptor.decrypt(entity.getAnswer()));
		model.setQuestionId(entity.getId());

		return model;
	}

	/**
	 * @param clientId
	 * @return
	 */
	public List<PersonSecInfoModel> getPersonSecurityInfo(Integer clientId) {
		ClientAuthentication client = validateClient(clientId);

		if (client == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal",
					String.valueOf(clientId));
		}

		List<PersonSecurityInfo> entityList = secInfoRepository.findByClient(client);

		if (entityList == null || entityList.isEmpty()) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.ENTITY_NOT_FOUND, "security info",
					String.valueOf(clientId));
		}
		
		List<PersonSecInfoModel> modelList = new ArrayList<PersonSecInfoModel>();
		for (PersonSecurityInfo entity : entityList) {
			PersonSecInfoModel model = new PersonSecInfoModel();
			model.setQuestion(encryptor.decrypt(entity.getQuestion()));
			model.setAnswer(encryptor.decrypt(entity.getAnswer()));
			model.setQuestionId(entity.getId());
			modelList.add(model);
		}

		return modelList;
	}

	/**
	 * @param clientId
	 * @param questionId
	 * @return
	 */
	public void deletePersonSecInfo(Integer clientId, Integer questionId) {
		if (clientId == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal",
					String.valueOf(clientId));
		}
		
		secInfoRepository.delete(questionId);
	}

	/**
	 * @param secInfoModel
	 * @param clientId
	 * @param questionId
	 * @return
	 */
	public PersonSecInfoModel updatePersonSecurityInfo(PersonSecInfoModel secInfoModel, Integer clientId, Integer questionId) {
		ClientAuthentication client = validateClient(clientId);

		if (client == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal",
					String.valueOf(clientId));
		}
		
		PersonSecurityInfo entity = secInfoRepository.findOne(questionId);

		if (entity == null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.ENTITY_NOT_FOUND, "security info",
					String.valueOf(questionId));
		}
		
		if (!StringUtils.isEmpty(secInfoModel.getAnswer())) {
			entity.setAnswer(encryptor.encrypt(secInfoModel.getAnswer()));
		}
		if (!StringUtils.isEmpty(secInfoModel.getQuestion())) {
			entity.setQuestion(encryptor.encrypt(secInfoModel.getQuestion()));
		}
		
		secInfoRepository.save(entity);
		
		return secInfoModel;
	}

}