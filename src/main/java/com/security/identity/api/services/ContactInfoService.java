package com.security.identity.api.services;

import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.entities.ContactInfo;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.ContactModel;

/**
 *
 * Provides a collection of all services related with ContactInfo
 * database object

 * @author Shubham Solanki
 * @Since 2018-02-10
 *
 * 
 */
@Service
public class ContactInfoService extends CommonService {
	
	/**
	 * Method to create contactInfo for specific client.
	 * 
	 * @param contact
	 * @param clientId
	 * @return
	 */
	public ContactModel createContactInfo(ContactModel contactModel, Integer clientId) {
		
		ClientAuthentication client = validateClient(clientId);
		
		if (client.getContactInfo() != null && client.getContactInfo().getContactId() != null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "contact", String.valueOf(client.getClientId())); 
		}
		
		if (client.getConfiguration().getContactInfo() != 1) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.UNAUTHORIZED_SERVICE, "contactInfo");
		}
		
		ContactInfo contact = saveContactInfoInDB(new ContactInfo(), contactModel, client);
		client.setContactInfo(contact);
		
		return convertContactEntityToModel(contact, client); 
	}
	
	/**
	 * Method to save contactInfo object in DB
	 * 
	 * @param contact
	 * @param contactModel
	 * @param client
	 * @return
	 */
	private ContactInfo saveContactInfoInDB(ContactInfo contact, ContactModel contactModel, ClientAuthentication client) {
		contact.setAddress1(contactModel.getPrimaryAddress());
		contact.setAddress2(contactModel.getSecondaryAddress());
		contact.setPhone(encryptor.encrypt(contactModel.getPhone()));
		contact.setEmailId1(encryptor.encrypt(contactModel.getPrimaryEmailId()));
		if (!StringUtils.isEmpty(contactModel.getSecondaryEmailId())) {
			contact.setEmailId2(encryptor.encrypt(contactModel.getSecondaryEmailId()));
		}
		
		// This is required before saving contact otherwise it is cascaded recursively and gives stack overflow error.
		if (client.getConfiguration() != null) {
			client.setConfiguration(null); 
		}
		if (client.getPersonalInfo() != null) {
			client.setPersonalInfo(null); 
		}
		contact.setClient(client);
		return contactRepository.save(contact);
	}

	/**
	 * Method to update contactInfo for specific client.
	 * 
	 * @param contactInfo
	 * @param clientId
	 * @return
	 * @throws ApplicationException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public ContactModel updateContactInfo(String contactJson, Integer clientId) throws ApplicationException, 
		JsonProcessingException, IOException {
		ContactModel contactModel = null;
		ClientAuthentication client = validateClient(clientId);
		
		if (client.getConfiguration().getContactInfo() != 1) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.UNAUTHORIZED_SERVICE, "contactInfo");
		}
		contactModel = convertContactEntityToModel(client.getContactInfo(), null);
		
		// Updated contactModel object
		contactModel = update(contactJson, contactModel);
		
		validateObject(contactModel);
		
		ContactInfo contact = saveContactInfoInDB(client.getContactInfo(), contactModel, client); 
		
		return convertContactEntityToModel(contact, client); 
	}
	
	/**
	 * Method to convert contact entity to contact model
	 * 
	 * @param contact
	 * @param client
	 * @return
	 */
	public ContactModel convertContactEntityToModel(ContactInfo contact, ClientAuthentication client) {
		ContactModel contactModel = new ContactModel();
		if (client != null) {
			contactModel.setClientId(client.getClientId()); 
			contactModel.setUserName(client.getUserName());
		}
		contactModel.setPrimaryAddress(contact.getAddress1());
		contactModel.setSecondaryAddress(contact.getAddress2());
		contactModel.setPhone(encryptor.decrypt(contact.getPhone())); 
		contactModel.setPrimaryEmailId(encryptor.decrypt(contact.getEmailId1()));
		contactModel.setPhone(encryptor.decrypt(contact.getPhone()));
		if (!StringUtils.isEmpty(contact.getEmailId2())) {
			contactModel.setSecondaryEmailId(encryptor.decrypt(contact.getEmailId2()));
		}
		return contactModel;
	}
	
}