package com.security.identity.api.services;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.entities.PersonalInfo;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.PersonalModel;

/**
 *
 * Provides a collection of all services related with PersonalInfo
 * database object

 * @author Shubham Solanki
 * @Since 2018-02-10
 *
 * 
 */
@Service
public class PersonalInfoService extends CommonService {
	
	/**
	 * Method to create personalInfo for a specific client
	 * 
	 * @param personalInfo
	 * @param clientId
	 * @return
	 */
	public PersonalModel createPersonalInfo(PersonalModel personalModel, Integer clientId) {
		
		ClientAuthentication client = validateClient(clientId);
		
		if (client.getPersonalInfo() != null && client.getPersonalInfo().getId() != null) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.NOT_ALLOWED, "personal", String.valueOf(client.getClientId())); 
		}
		
		if (client.getConfiguration().getPersonalInfo() != 1) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.UNAUTHORIZED_SERVICE, "personalInfo");
		}
		
		PersonalInfo personalInfo = savePersonalInfoInDB(new PersonalInfo(), personalModel, client);
		client.setPersonalInfo(personalInfo); 

		return convertPersonalEntityToModel(personalInfo, client);
	}
	
	/**
	 * Method to save personalInfo object in DB
	 * 
	 * @param personalInfo
	 * @param personalModel
	 * @param client
	 * @return
	 */
	private PersonalInfo savePersonalInfoInDB(PersonalInfo personalInfo, PersonalModel personalModel, ClientAuthentication client) {
		personalInfo.setFirstName(personalModel.getFirstName());
		personalInfo.setMiddleName(personalModel.getMiddleName());
		personalInfo.setLastName(personalModel.getLastName());
		personalInfo.setSpouseFirstName(personalModel.getSpouseFirstName());
		personalInfo.setSpouseMiddleName(personalModel.getSpouseMiddleName());
		personalInfo.setSpouseLastName(personalModel.getSpouseLastName());
		personalInfo.setAge(personalModel.getAge()); 
		
		// This is required before saving contact otherwise it is cascaded recursively and gives stack overflow error.
		if (client.getConfiguration() != null) {
			client.setConfiguration(null); 
		}
		if (client.getContactInfo() != null) {
			client.setContactInfo(null);  
		}
		personalInfo.setClient(client); 
		return personalRepository.save(personalInfo);
	}

	/**
	 * Method to convert personalInfo entity to model
	 * 
	 * @param personalInfo
	 * @param client
	 * @return
	 */
	public PersonalModel convertPersonalEntityToModel(PersonalInfo personalInfo, ClientAuthentication client) {
		PersonalModel model = new PersonalModel();
		if (client != null) {
			model.setClientId(client.getClientId()); 
			model.setUserName(client.getUserName());
		}
		model.setFirstName(personalInfo.getFirstName());  
		model.setMiddleName(personalInfo.getMiddleName());
		model.setLastName(personalInfo.getLastName());
		model.setSpouseFirstName(personalInfo.getSpouseFirstName());
		model.setSpouseMiddleName(personalInfo.getSpouseMiddleName());
		model.setSpouseLastName(personalInfo.getSpouseLastName());
		model.setAge(personalInfo.getAge());
		return model;
	}

	/**
	 * Method to update personalInfo for a specific client
	 * 
	 * @param personalInfo
	 * @param clientId
	 * @return
	 * @throws ApplicationException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PersonalModel updatePersonalInfo(String personalJson, Integer clientId) throws ApplicationException, 
		JsonProcessingException, IOException {
		PersonalModel personalModel = null;
		ClientAuthentication client = validateClient(clientId);
		
		if (client.getConfiguration().getPersonalInfo() != 1) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.UNAUTHORIZED_SERVICE, "personalInfo");
		}
		personalModel = convertPersonalEntityToModel(client.getPersonalInfo(), null);
		
		// Updated personalModel object
		personalModel = update(personalJson, personalModel);
		
		validateObject(personalModel);
		
		PersonalInfo personalInfo = savePersonalInfoInDB(client.getPersonalInfo(), personalModel, client);
		
		return convertPersonalEntityToModel(personalInfo, client);
	}

}