package com.security.identity.api.services;

import static com.security.identity.api.response.MessageUtil.getMessageFromBundle;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.models.ClientModel;
import com.security.identity.api.response.ApiResponse;

/**
 *
 * Provides a collection of services related with password settings.

 * @author Shubham Solanki
 * @Since 2018-02-10
 *
 * 
 */
@Service
public class PasswordSettingsService extends CommonService {
	
	/**
	 * Method to reset the password into the application.
	 * 
	 * @param clientModel
	 * @return
	 */
	@Transactional
	public ApiResponse updatePassword(ClientModel clientModel) {
		String userName = getAuthenticatedPrincipal();
		ClientAuthentication client = clientRepository.findByUserName(userName);
		if (!client.getUserName().equals(clientModel.getUserName())) { 
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.RESOURCE_NOT_ACCESSIBLE);
		}
		
		if (client.getConfiguration().getPasswordSettings() != 1) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.UNAUTHORIZED_SERVICE, "passwordSettings");
		}
		
		validatePassword(clientModel, client, true);
		
		client.setPassword(encoder.encode(clientModel.getPassword()));  
		clientRepository.save(client);
		
		ApiResponse response = new ApiResponse();
		response.setMessage(getMessageFromBundle(messageHandler, ApiErrorConstants.SUCCESSFULLY_UPDATED));
		return response;
	}

}