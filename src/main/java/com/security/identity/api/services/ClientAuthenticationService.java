package com.security.identity.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.repositories.ClientRepository;

/**
 * This class is used to load client-specific data for client authentication.
 * 
 * @author Shubham Solanki
 * @since 2018-23-02
 *
 */
@Service
public class ClientAuthenticationService implements UserDetailsService {

	@Autowired
	private ClientRepository clientRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails
	 * UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ClientAuthentication client = clientRepository.findByUserName(username);

		if (null == client) {
			throw new UsernameNotFoundException("No client is found with this username " + username);
		}
		return new User(client.getUserName(), client.getPassword(), AuthorityUtils.NO_AUTHORITIES);
	}

}