package com.security.identity.api.services;

import java.io.IOException;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.security.identity.api.encryptor.AESEncryptorDecryptor;
import com.security.identity.api.entities.ClientAuthentication;
import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;
import com.security.identity.api.exceptions.MessageSourceHandler;
import com.security.identity.api.models.ClientModel;
import com.security.identity.api.repositories.ClientRepository;
import com.security.identity.api.repositories.ConfigurationRepository;
import com.security.identity.api.repositories.ContactInfoRepository;
import com.security.identity.api.repositories.PersonSecurityInfoRepository;
import com.security.identity.api.repositories.PersonalInfoRepository;

/**
 * Common Service to hold all general operations
 * 
 * @author Shubham Solanki
 * @Since 2018-02-10
 *
 */
@Service
public class CommonService {

	@Autowired
	protected ClientRepository clientRepository;

	@Autowired
	protected ConfigurationRepository configRepository;

	@Autowired
	protected ContactInfoRepository contactRepository;

	@Autowired
	protected PersonalInfoRepository personalRepository;

	@Autowired
	protected PasswordEncoder encoder;

	@Autowired
	protected AESEncryptorDecryptor encryptor;

	@Autowired
	protected MessageSourceHandler messageHandler;

	@Autowired
	protected PersonSecurityInfoRepository secInfoRepository;

	/**
	 * Method to validate the password.
	 * 
	 * @param userModel
	 * @param user
	 * @param isUpdate
	 */
	protected void validatePassword(ClientModel clientModel, ClientAuthentication client, boolean isUpdate) {
		if (isUpdate) {
			if (StringUtils.isEmpty(clientModel.getOldPassword())) {
				throw ApplicationException.createBadRequest(ApiErrorConstants.PASSWORD_NOT_EMPTY);
			}
			if (!encoder.matches(clientModel.getOldPassword(), client.getPassword())) {
				throw ApplicationException.createBadRequest(ApiErrorConstants.INVALID_PASSWORD, "client",
						"password=" + clientModel.getOldPassword());
			}
		}
		if (!clientModel.getPassword().equals(clientModel.getConfirmPassword())) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.PASSWORDS_NOT_SAME, "password",
					"confirmPassword");
		}
	}

	/**
	 * Method to validate whether client exists or not
	 * 
	 * @param clientId
	 * @return
	 */
	protected ClientAuthentication validateClient(Integer clientId) {
		String userName = getAuthenticatedPrincipal();
		ClientAuthentication client = clientRepository.findByUserName(userName);
		if (!client.getClientId().equals(clientId)) {
			throw ApplicationException.createUnauthorizedError(ApiErrorConstants.RESOURCE_NOT_ACCESSIBLE);
		}
		return client;
	}

	/**
	 * This function overrides values from given json string into given
	 * objectToUpdate
	 * 
	 * @param json
	 * @param objectToUpdate
	 * @return
	 * @throws IOException
	 */
	protected <T> T update(String json, T objectToUpdate) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setDefaultMergeable(true); // This is required for deep
												// update. Available in
												// jackson-databind from 2.9
												// version
		ObjectReader updater = objectMapper.readerForUpdating(objectToUpdate);

		return updater.readValue(json);
	}

	/**
	 * Validates given object for any constraint violations and throws
	 * ConstraintViolationException if any violations are found
	 * 
	 * @param object
	 */
	public <T> void validateObject(T object) {

		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

		Set<ConstraintViolation<T>> constraintViolations = validator.validate(object);

		if (constraintViolations != null && !constraintViolations.isEmpty()) {
			ConstraintViolationException ex = new ConstraintViolationException(constraintViolations);
			throw ex;
		}
	}

	/**
	 * Method to validate userName from DB
	 * 
	 * @param userName
	 */
	public void validateUserName(String userName) {
		ClientAuthentication client = clientRepository.findByUserName(userName);
		if (null != client) {
			throw ApplicationException.createBadRequest(ApiErrorConstants.USER_ALREADY_EXISTS, userName);
		}
	}

	/**
	 * Method to get principal username authenticated by spring security layer.
	 * 
	 * @return
	 */
	protected String getAuthenticatedPrincipal() {
		return ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
	}

}