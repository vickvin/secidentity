package com.security.identity.api.encryptor;

import static com.security.identity.api.ApplicationConstants.AES_ALGO;
import static com.security.identity.api.ApplicationConstants.AES_PKC_PADDING;
import static com.security.identity.api.ApplicationConstants.UTF8;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.security.identity.api.exceptions.ApiErrorConstants;
import com.security.identity.api.exceptions.ApplicationException;

import lombok.Data;

/**
 * Encryption\Decryption with use of AES algorithm
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@Data
public class AESEncryptorDecryptor {
    
    private Cipher encipher;
    private Cipher decipher;
    
    /**
     * Constructor
     * 
     * @param key
     * @param initVector
     */
    public AESEncryptorDecryptor(String key, String initVector) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(UTF8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(UTF8), AES_ALGO);
            encipher = Cipher.getInstance(AES_PKC_PADDING);
            encipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            decipher = Cipher.getInstance(AES_PKC_PADDING);
            decipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        } catch (Exception ex) {
            throw ApplicationException.createEncryptionError(ApiErrorConstants.ENCRYPTION_ERROR, ex.getMessage());
        }
    }

    /**
     * This method is used to encrypt the value.
     * 
     * @param value
     * @return
     */
    public String encrypt(String value) {
        try {
            byte[] encrypted = encipher.doFinal(value.getBytes());
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            throw ApplicationException.createEncryptionError(ApiErrorConstants.ENCRYPTION_ERROR, ex.getMessage());
        }
    }

    /**
     * This method is used to decrypt the encrypted value.
     * 
     * @param encryptedValue
     * @return
     */
    public String decrypt(String encryptedValue) {
        try {
            byte[] original = decipher.doFinal(Base64.getDecoder().decode(encryptedValue));
            return new String(original);
        } catch (Exception ex) {
            throw ApplicationException.createEncryptionError(ApiErrorConstants.DECRYPTION_ERROR, ex.getMessage());
        }
    }
    
}