package com.security.identity.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.security.identity.api.encryptor.AESEncryptorDecryptor;

/**
 * Main class for Spring Boot based API application.
 * 
 * @author Shubham Solanki
 * @since 2018-02-10
 *
 */
@SpringBootApplication
public class Application {
	
	@Value("${com.security.identity.api.encryptor.encrypt.key}")
	private String key;
	
	@Value("${com.security.identity.api.encryptor.initVector}")
	private String initVector;
	
	
	/**
     * Main method for spring application
     * 
     * @param args command line arguments passed to app
     * 
     */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/**
     * Facilitates hashing algorithm for password field.
     * 
     * @return PasswordEncoder
     */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	/**
     * Facilitates Encryption technique for various other fields (phone, email etc.).
     * 
     * @return AESEncryptorDecryptor
     */
	@Bean
	public AESEncryptorDecryptor getEncryptor() {
		return new AESEncryptorDecryptor(key, initVector);
	}
	
	/**
     * Facilitates messageSoruce
     * 
     * @return MessageSource
     */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}
	
}