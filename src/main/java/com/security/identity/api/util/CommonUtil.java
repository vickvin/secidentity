package com.security.identity.api.util;

import static com.security.identity.api.ApplicationConstants.VALID_FORMAT_YYYY_MM_DD;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class to keep some common util methods 
 * 
 * @author Shubham Solanki
 * @since 2018-23-02
 *
 */
public class CommonUtil {
	
	/**
     * This will return current date and time in UTC
     * 
     * @return
     */
    public static String getTodayInUTC() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern(VALID_FORMAT_YYYY_MM_DD);
        ZonedDateTime utcDateTime = ZonedDateTime.now(ZoneOffset.UTC);
        return format.format(utcDateTime);
    }

}
