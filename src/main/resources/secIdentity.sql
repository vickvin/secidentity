/*
SQLyog - Free MySQL GUI v5.17
Host - 5.7.21 : Database - registration
*********************************************************************
Server version : 5.7.21
*/

SET NAMES utf8;

SET SQL_MODE='';

create database if not exists `sec_identity`;

USE `sec_identity`;

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

/*Table structure for table `authentication` */

DROP TABLE IF EXISTS `authentication`;

CREATE TABLE `authentication` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `contact_info` */

DROP TABLE IF EXISTS `contact_info`;

CREATE TABLE `contact_info` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `address1` varchar(200) NOT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `email_id1` varchar(255) NOT NULL,
  `email_id2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `FK_contact_authentication` (`user_id`),
  CONSTRAINT `FK_contact_authentication` FOREIGN KEY (`user_id`) REFERENCES `authentication` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `personal_info` */

DROP TABLE IF EXISTS `personal_info`;

CREATE TABLE `personal_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `spouse_first_name` varchar(50) DEFAULT NULL,
  `spouse_last_name` varchar(50) DEFAULT NULL,
  `spouse_middle_name` varchar(50) DEFAULT NULL,
  `age` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_personal_authentication` (`user_id`),
  CONSTRAINT `FK_personal_authentication` FOREIGN KEY (`user_id`) REFERENCES `authentication` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `user_configuration` */

DROP TABLE IF EXISTS `user_configuration`;

CREATE TABLE `user_configuration` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_personal_info` tinyint(1) DEFAULT '0',
  `user_contact_info` tinyint(1) DEFAULT '0',
  `password_settings` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`config_id`),
  KEY `FK_config_authentication` (`user_id`),
  CONSTRAINT `FK_config_authentication` FOREIGN KEY (`user_id`) REFERENCES `authentication` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

/*Table structure for table `person_security_info` */

DROP TABLE IF EXISTS `person_security_info`;

CREATE TABLE `person_security_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `question` varchar(500) NOT NULL,
  `answer` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_secInfo_userId` (`user_id`),
  CONSTRAINT `FK_secInfo_userId` FOREIGN KEY (`user_id`) REFERENCES `authentication` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
